#pragma once
//#include "Ship.h"



class board
{
public:
	board();
	~board();

private:
	// xxxx _board;
	
};

/*
example for 5X5: ?

    1   2   3   4   5

a   O   O   O   X   O

b   O   X   O   O   O

c   O   O   O   O   O

d   O   O   O   *   O

e   O   O   O   O   O


a4 - miss
b2 - miss
d4 - hit

if we realy want to make an effort .. 

idea 1:
	we can make it so that if you already know: 
		1. if the battleship is horizontal or vertical
		2. and you know that a sertain point is the edge of the ship
	we can draw instead of a '*' a 'v' / '^' / '>' / '<' at the edge
	and the body would be or a '-' or a '|'

	sort of like this:

		1   2   3   4   5

	a   ^   X   O   *   O

	b   O   X   O   O   O

	c   O   O   -   >   X

	d   O   O   O   O   O

	e   O   <   -   >   O

	a1 - hit, a2 - miss
	the ship can only be verticaly downwards therefore the '^' instead of the '*'

	a4 - hit
	nothing to know about placement

	c3 - hit, c4 - hit, c5 - miss
	the ship is horizontal and can't continue to the right therefore the '>'

	e2, e3, e4 - all hits - ship sunk
	entire ship down therefore the '< - >' instead of the '- - -'

idea 2:
	when you sink an entire ship
	play an explosion sound (dumb - realy dumb - and very much not mandatory)
	mark with misses (X) the adjacent locations


TL;DR:

1.
	(signs are negotiable...)

					  'O' - nothing happend there yet
					  'X' - miss
					  '*' - hit

				'-' / '|' - middle of a ship 
	'<' / '>' / 'V' / '^' - edge of a ship

	two final things realy not mandatory, and if we actualy will code it, it'll be in the end.

2.
	at ship sink event - mark with misses all the surrounding points.

*/
